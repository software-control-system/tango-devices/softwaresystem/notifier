package fr.soleil.notification;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;

import fr.soleil.tango.clientapi.TangoGroupCommand;

public class TextTalkerNotification implements INotification {
    private final Logger logger = LoggerFactory.getLogger(TextTalkerNotification.class);
    private static final String COMMAND = "DevTalk";
    private final TangoGroupCommand tangoGroupCommand;
    private String message = "";

    public TextTalkerNotification(final List<String> fullDevicesNames) throws NotificationException {

        try {
            this.tangoGroupCommand = new TangoGroupCommand("TextTalkerNotification", COMMAND,
                    fullDevicesNames.toArray(new String[fullDevicesNames.size()]));
        } catch (DevFailed e) {
            throw new NotificationException(e);
        }
    }

    @Override
    public void emit() throws NotificationException {

        try {
            this.tangoGroupCommand.insert(this.message);
            this.tangoGroupCommand.execute();
        } catch (DevFailed e) {
            DevFailedUtils.logDevFailed(e, this.logger);
            throw new NotificationException(e);
        }
    }

    @Override
    public void setMessage(final String message) {
        this.message = message;

    }
}
