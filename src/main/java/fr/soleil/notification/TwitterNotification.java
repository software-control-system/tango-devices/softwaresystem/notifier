package fr.soleil.notification;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;

@Deprecated()
public class TwitterNotification implements INotification {
    private long idUser = -1;
    /**
     * The authentification user for the server
     */
    private String authUser = null;
    /**
     * The authentification password for the server
     */
    private String authPasswd = null;
    /**
     * The message of the tweet
     */
    private String message = "";

    @Override
    public void emit() {
	final ConfigurationBuilder cb = new ConfigurationBuilder();
	cb.setDebugEnabled(false).setOAuthConsumerKey("Ql7Na8ndeqQJDde9R1Q")
		.setOAuthConsumerSecret("iQ0ZX402WwpziOR7ZoIYso7NqlrMyn5ncveBWKABB38")
		.setHttpProxyHost("195.221.0.6").setHttpProxyPort(8080)
		.setHttpProxyUser("fourneau").setHttpProxyPassword("xxx"); // XXX
	final TwitterFactory tf = new TwitterFactory(cb.build());
	final Twitter twitter = tf.getInstance();
	try {
	    final RequestToken requestToken = twitter.getOAuthRequestToken();
	    AccessToken accessToken = null;
	    if (this.authUser != null) {
		accessToken = new AccessToken(this.authUser, this.authPasswd, this.idUser);
		twitter.setOAuthAccessToken(accessToken);
	    }
	    final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	    while (null == accessToken) {
		System.out.println("Open the following URL and grant access to your account:");
		System.out.println(requestToken.getAuthorizationURL());
		System.out.print("Enter the PIN(if aviailable) or just hit enter.[PIN]:");
		final String pin = br.readLine();
		try {
		    if (pin.length() > 0) {
			accessToken = twitter.getOAuthAccessToken(requestToken, pin);
		    } else {
			accessToken = twitter.getOAuthAccessToken();
		    }
		    this.storeAccessToken(twitter.verifyCredentials().getId(), accessToken);
		} catch (final TwitterException te) {
		    if (401 == te.getStatusCode()) {
			System.out.println("Unable to get the access token.");
		    } else {
			te.printStackTrace();
		    }
		}
	    }
	    // persist to the accessToken for future reference.

	    final Status status = twitter.updateStatus(this.message);

	    System.out.println("Successfully updated the status to [" + status.getText() + "].");
	} catch (final TwitterException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (final IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

    private void storeAccessToken(final long l, final AccessToken accessToken) {
	this.authUser = accessToken.getToken();
	this.authPasswd = accessToken.getTokenSecret();
	this.idUser = l;
    }

    @Override
    public void setMessage(final String message) {
	this.message = message;

    }

    // @Override
    public void setAuthentification(final String authUser, final String authPasswd) {
	this.authUser = authUser;
	this.authPasswd = authPasswd;

    }

    public static void main(final String[] args) {
	final TwitterNotification t = new TwitterNotification();
	t.setMessage("pouet");
	t.emit();
	System.out.println("wait");
	t.setMessage("that work");
	t.emit();
    }

}
