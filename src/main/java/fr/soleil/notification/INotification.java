package fr.soleil.notification;

import fr.esrf.Tango.DevFailed;

/**
 * 
 * Permit to send a notification
 * 
 * @author Fourneau
 * 
 */
public interface INotification {
    /**
     * Send a notification
     * 
     * @throws DevFailed
     */
    void emit() throws NotificationException;

    //
    // /**
    // * Set a subject to the notification
    // *
    // * @param subject
    // */
    // void setSubject(String subject);
    //
    /**
     * Set the message of the notification
     * 
     * @param message
     */
    void setMessage(String message);
    //
    // /**
    // * Set the recipients of the notification.
    // *
    // * @param recipients
    // */
    // void setRecipient(String... recipients);
    //
    // /**
    // * Set an authentification for the notification
    // *
    // * @param authUser
    // * @param authPasswd
    // */
    // void setAuthentification(String authUser, String authPasswd);

}
