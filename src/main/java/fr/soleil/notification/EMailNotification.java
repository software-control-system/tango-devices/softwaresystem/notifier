package fr.soleil.notification;

import java.io.File;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Fourneau
 * 
 */
public class EMailNotification implements INotification {
    private final Logger logger = LoggerFactory.getLogger("Notification");
    /**
     * The email adress of the default emitter
     */
    private static final String DEFAULT_EMITTER = "tango@synchrotron-soleil.fr";
    /**
     * The default mail subject
     */
    private static final String DEFAULT_SUBJECT = "Default subject";
    /**
     * The default mail message
     */
    private static final String DEFAULT_MESSAGE = "Default message";
    /**
     * The default mime type of the email
     */
    private static final String DEFAULT_MIME = "text/plain";
    /**
     * The HTML mime for html content email
     */
    private static final String HTML_MIME = "text/html";

    /**
     * The emitter of the mail
     */
    private String emitter = DEFAULT_EMITTER;
    /**
     * The main recipients of the mail
     */
    private String[] recipientsTO = new String[0];
    /**
     * The "Cc" (carbon copy) recipients of the mail
     */
    private String[] recipientsCC = new String[0];
    /**
     * The "BCc" (blind carbon copy) recipients of the mail
     */
    private String[] recipientsBCC = new String[0];
    /**
     * The subject of the mail
     */
    private String subject = DEFAULT_SUBJECT;
    /**
     * The message of the mail
     */
    private String message = DEFAULT_MESSAGE;
    /**
     * The attached documents of the mail
     */
    private final Map<String, String> attachedDocuments = new HashMap<String, String>();
    /**
     * The authentification for the mail server
     */
    private Authenticator auth = null;
    /**
     * The server mail
     */
    private String server;
    /**
     * The authentification user for the server
     */
    private String authUser;
    /**
     * The authentification password for the server
     */
    private String authPasswd;
    /**
     * The mime type of the mail
     */
    private String mimeType;

    /**
     * The default constructor with default address server :
     * sun-owa.synchrotron-soleil.fr
     */
    public EMailNotification() {
        this("sun-owa.synchrotron-soleil.fr");
    }

    /**
     * Constructor
     * 
     * @param server
     *            the address of the mail server
     */
    public EMailNotification(final String server) {
        this.server = server;
    }

    @Override
    public final void emit() throws NotificationException {
        logger.debug("sending email with subject [{}] to {}", subject, Arrays.toString(recipientsTO));
        Transport transport = null;
        final Properties prop = System.getProperties();
        prop.put("mail.transport.protocol", "smtp");
        prop.put("mail.smtp.host", this.server);
        if (this.auth != null) {
            prop.put("mail.smtp.auth", "true");
        }
        final Session session = Session.getInstance(prop, this.auth);
        // If you want logs
        // session.setDebug(true);
        final Message messageWithHeader = new MimeMessage(session);
        try {
            final Multipart multipart = new MimeMultipart();
            BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(this.message, this.mimeType);
            multipart.addBodyPart(messageBodyPart);
            messageWithHeader.setSubject(this.subject);
            messageWithHeader.addHeader("X-Mailer", "Java");
            messageWithHeader.addHeader("Reply-To", this.emitter);

            messageWithHeader.setSentDate(new Date());
            messageWithHeader.setFrom(new InternetAddress(this.emitter));
            for (final String recipients : this.recipientsTO) {
                messageWithHeader.addRecipient(Message.RecipientType.TO, new InternetAddress(recipients));
            }
            for (final String recipients : this.recipientsCC) {
                messageWithHeader.addRecipient(Message.RecipientType.CC, new InternetAddress(recipients));
            }
            for (final String recipients : this.recipientsBCC) {
                messageWithHeader.addRecipient(Message.RecipientType.BCC, new InternetAddress(recipients));
            }

            for (final Entry<String, String> entry : this.attachedDocuments.entrySet()) {
                messageBodyPart = new MimeBodyPart();
                final DataSource source = new FileDataSource(entry.getKey());
                messageBodyPart.setDataHandler(new DataHandler(source));
                messageBodyPart.setFileName(entry.getValue());
                multipart.addBodyPart(messageBodyPart);
            }
            messageWithHeader.setContent(multipart);

            transport = session.getTransport();
            if (this.auth == null) {
                transport.connect();
            } else {
                transport.connect(this.server, this.authUser, this.authPasswd);
            }
            transport.sendMessage(messageWithHeader, messageWithHeader.getAllRecipients());
        } catch (final AddressException e) {
            logger.error(e.getMessage());
            throw new NotificationException(e);

        } catch (final MessagingException e) {
            logger.error(e.getMessage());
            throw new NotificationException(e);

        } finally {
            if (transport != null) {
                try {
                    transport.close();
                } catch (final MessagingException e) {
                    // ignore
                }
            }
        }
    }

    public final String getEmitter() {
        return this.emitter;
    }

    public final void setEmitter(final String emitter) {
        this.emitter = emitter;
    }

    public final void setAuthentification(final String authUser, final String authPasswd) {
        // TODO check this
        // i'm not sure that the authUser for autentification is the email
        // address of the emitter on AUTH-SMTP
        this.setEmitter(authUser);
        this.auth = new Authenticator() {
            @Override
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(authUser, authPasswd);
            }
        };
        this.authUser = authUser;
        this.authPasswd = authPasswd;
    }

    public final String[] getPrimaryRecipients() {
        return Arrays.copyOf(this.recipientsTO, this.recipientsTO.length);
    }

    public final void setPrimaryRecipients(final String... recipients) {
        this.recipientsTO = recipients;
    }

    public final String[] getCCRecipients() {
        return Arrays.copyOf(this.recipientsCC, this.recipientsCC.length);
    }

    public final void setCCRecipients(final String... recipients) {
        this.recipientsCC = recipients;
    }

    public final String[] getBlindCCRecipients() {
        return Arrays.copyOf(this.recipientsBCC, this.recipientsBCC.length);
    }

    public final void setBlindCCRecipients(final String... recipients) {
        this.recipientsBCC = recipients;
    }

    public final String getSubject() {
        return this.subject;
    }

    public final void setSubject(final String subject) {
        this.subject = subject;
    }

    public final String getMessage() {
        return this.message;
    }

    public final void setTextMessage(final String message) {
        this.message = message;
        this.mimeType = DEFAULT_MIME;
    }

    public final void setHtmlMessage(final String message) {
        this.message = message;
        this.mimeType = HTML_MIME;
    }

    public final void addAttachedDocument(final String location) {
        this.addAttachedDocument(location, location.substring(location.lastIndexOf(File.separator) + 1));
    }

    public final void addAttachedDocument(final String location, final String name) {
        this.attachedDocuments.put(location, name);
    }

    // @Override
    @Override
    public final void setMessage(final String message) {
        this.setTextMessage(message);
    }

    // @Override
    public final void setRecipient(final String... recipients) {
        this.setPrimaryRecipients(recipients);
    }

    public final String getServer() {
        return this.server;
    }

    public final void setServer(final String server) {
        this.server = server;
    }

    /**
     * Reset all the EMailNotification except the address of the mail server
     */
    public final void reset() {
        this.emitter = DEFAULT_EMITTER;
        this.message = DEFAULT_MESSAGE;
        this.subject = DEFAULT_SUBJECT;
        this.mimeType = DEFAULT_MIME;
        this.recipientsTO = new String[0];
        this.recipientsCC = new String[0];
        this.recipientsBCC = new String[0];
        this.attachedDocuments.clear();
        this.auth = null;
        this.authUser = "";
        this.authPasswd = "";
    }

    public static void main(final String[] args) {
        /*
         * Short example
         */// "192.168.214.254"
        final EMailNotification e = new EMailNotification("192.168.212.105");
        // e.setAuthentification("a.sineau@codra.fr", "as");
        e.setSubject("Test");
        e.setHtmlMessage("<H1>bonjour</H1><a href=\"google.fr\">google</a> <p> click it</p>");
        e.setPrimaryRecipients("a.sineau@codra.fr");
        // e.addAttachedDocument("N:\\ControleCommande\\Tango\\Archiving\\Collaboration_MAX_IV\\EN_CDCF_Extension Watcher TDB.docx");
        e.setEmitter("a.sineau@codra.fr");
        try {
            e.emit();
        } catch (final NotificationException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
    }
}