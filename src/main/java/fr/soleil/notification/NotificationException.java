package fr.soleil.notification;


public class NotificationException extends Exception {

    public NotificationException(final Exception exception) {
        super(exception.getMessage(), exception);
    }
}
