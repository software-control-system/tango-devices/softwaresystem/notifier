package fr.soleil.tango.server.notifier;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.DeviceState;
import org.tango.server.annotation.Command;
import org.tango.server.annotation.Delete;
import org.tango.server.annotation.Device;
import org.tango.server.annotation.DeviceProperty;
import org.tango.server.annotation.Init;
import org.tango.server.annotation.Schedule;
import org.tango.server.annotation.StateMachine;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.soleil.tango.clientapi.InsertExtractUtils;
import fr.soleil.tango.clientapi.TangoAttribute;
import fr.soleil.tango.clientapi.TangoCommand;
import fr.soleil.tango.clientapi.TangoGroupAttribute;

@Device
public class EventMonitor {

    private final Logger loggerDevice = LoggerFactory.getLogger(Notifier.LOGGER_NAME);
    private final Logger logger = LoggerFactory.getLogger(EventMonitor.class);

    @DeviceProperty
    private String[] eventAttributes;

    @DeviceProperty
    private String[] emailRecipients;

    private final Map<String, Boolean> attributes = new HashMap<String, Boolean>();

    private TangoGroupAttribute monitoredAttributes;

    private TangoCommand sendEmail;

    private final static String EMAIL_SUBJECT = "event occured";

    @Init
    @StateMachine(endState = DeviceState.ON)
    public void init() throws DevFailed {
        for (int i = 0; i < eventAttributes.length; i++) {
            final TangoAttribute att = new TangoAttribute(eventAttributes[i]);
            if (!att.isBoolean()) {
                throw DevFailedUtils.newDevFailed("only boolean attributes are supported");
            }
            if (!att.isScalar()) {
                throw DevFailedUtils.newDevFailed("only scalar attributes are supported");
            }
            attributes.put(eventAttributes[i], false);
        }
        monitoredAttributes = new TangoGroupAttribute(false, eventAttributes);
        sendEmail = new TangoCommand(Notifier.getFirstNotifier(), "sendEmail");
        loggerDevice.info("monitoring {}", Arrays.toString(eventAttributes));
    }

    @Delete
    public void delete() {
        attributes.clear();
    }

    @Command
    @Schedule(cronExpression = "* * * * * ?", activationProperty = "checkEventsPeriod")
    public void checkEvents() {
        logger.debug("checking events {}", Arrays.toString(eventAttributes));
        DeviceAttribute[] results;
        try {
            results = monitoredAttributes.read();
        } catch (final DevFailed e) {
            loggerDevice.error("could not read attributes", e);
            loggerDevice.error(DevFailedUtils.toString(e));
            return;
        }
        int i = 0;
        final List<String> argin = new LinkedList<String>();
        argin.add(EMAIL_SUBJECT);
        final StringBuilder sb = new StringBuilder();
        for (final Entry<String, Boolean> entry : attributes.entrySet()) {
            final String attributeName = entry.getKey();
            final boolean previousEvent = entry.getValue();
            try {
                final boolean event = InsertExtractUtils
                        .extractRead(results[i++], AttrDataFormat.SCALAR, boolean.class);
                logger.debug("checking event={}, previous={}", event, previousEvent);
                if (previousEvent != event) {
                    // an event occurred
                    sb.append(attributeName).append(" = ").append(event).append("\n\n");
                }
                entry.setValue(event);
            } catch (final DevFailed e) {
                loggerDevice.error("could not get attribute value", e);
                loggerDevice.error(DevFailedUtils.toString(e));
            }
        }

        if (!sb.toString().isEmpty()) {
            argin.add(sb.toString());
            // send email only if at least an event occurred
            argin.addAll(Arrays.asList(emailRecipients));
            logger.debug("sendEmail {}", argin);
            try {
                sendEmail.execute(argin.toArray(new String[argin.size()]));
            } catch (final DevFailed e) {
                loggerDevice.error("could not send emails", e);
                loggerDevice.error(DevFailedUtils.toString(e));
            }
        }
    }

    public void setEventAttributes(final String[] eventAttributes) {
        this.eventAttributes = eventAttributes;
    }

    public void setEmailRecipients(final String[] emailRecipients) {
        this.emailRecipients = emailRecipients;
    }

}
